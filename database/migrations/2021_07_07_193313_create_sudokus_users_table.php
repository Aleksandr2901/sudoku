<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSudokusUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sudoku_user', function (Blueprint $table) {
            $table->foreignId("user_id")
                ->constrained("users")
                ->cascadeOnDelete();
            $table->foreignId("sudoku_id")
                ->constrained("sudokus")
                ->cascadeOnDelete();
            $table->primary(["user_id","sudoku_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sudokus_users');
    }
}

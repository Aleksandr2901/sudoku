<?php

namespace App\Providers;

use App\Models\Sudoku;
use App\Models\User;
use App\Observers\SudokuObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Sudoku::observe([
            SudokuObserver::class,
        ]);
        User::observe([
            UserObserver::class
        ]);
    }
}

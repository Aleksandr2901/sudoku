<?php

namespace App\Services;

const sudokuSquares = [
    [0, 1, 2, 9, 10, 11, 18, 19, 20],
    [3, 4, 5, 12, 13, 14, 21, 22, 23],
    [6, 7, 8, 15, 16, 17, 24, 25, 26],
    [27, 28, 29, 36, 37, 38, 45, 46, 47],
    [30, 31, 32, 39, 40, 41, 48, 49, 50],
    [33, 34, 35, 42, 43, 44, 51, 52, 53],
    [54, 55, 56, 63, 64, 65, 72, 73, 74],
    [57, 58, 59, 66, 67, 68, 75, 76, 77],
    [60, 61, 62, 69, 70, 71, 78, 79, 80]
];

class  SudokuService
{
    private $repeat = true;
    private $solutions;
    private $segments;

    // сложность
    // новое поле

    public static function sudokuSolutionDifficulty(string $stringField, string $answer = null): int
    {
        $str1 = $stringField;
        $methods = new SudokuService();
        $methods->solutions = 'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq';
        $methods->BulkheadZeros($stringField, $answer);
        $data = array_diff_assoc(str_split($str1), str_split($methods->solutions));
        return count($data);

    }

    public function BulkheadZeros(string $stringField, string $answer = null)
    {
//        if ($stringField == '000000200000060079180007006000000001000002000003000045000400008007000000206030004') {
//            dump('hi');
//        }
        $field = $this->fieldInit($stringField);
        if ($this->checkWin($field)) {
//            dump($stringField);
//            dump(substr_count($stringField, 0));
            if (substr_count($stringField, 0) > substr_count($this->solutions, 0)) {

//                dump(substr_count($this->solutions, 0));
//                dump($stringField);
                $this->solutions = $stringField;
            }
        } else if (!$this->checkLose($field) &&
            substr_count($this->solutions, 0) < substr_count($stringField, 0)) {
            $ar = [];
            $count = 2;
            while (count($ar) ===0) {
                $ar = array_filter($field, function ($value) use ($count) {
                    return count($value['possibly']) === $count;
                });
                $count++;
//                dump($stringField);
//                dump($ar);
            }
//            $ar = array_filter($field, function ($value) {
//                return count($value['possibly']) >1;
//            });
//            $str = $stringField;
//            if (substr_count($this->solutions, 0) < substr_count($str, 0)) {
//            $elem = reset($ar);
                foreach ($ar as $elem) {
//                    if ($elem['id'] ===4) {
//                        dump($stringField);
//                        dump('hi');
//                        dump(substr_count($stringField, 0));
//
//                    }
                    if ($answer) {
                        $str = $stringField;
                        $str[$elem['id']] = $answer[$elem['id']];
                        $this->BulkheadZeros($str, $answer);
                    } else {
                        foreach ($elem['possibly'] as $item) {
                            $str = $stringField;
//                            dump($str);
                            $str[$elem['id']] = $item;
//                            dump($str);
                            $this->BulkheadZeros($str, $answer);
                        }
                    }
                }
        }
    }

    public function fieldInit(string $stringField)
    {
        $array = [];
        for ($i = 0; $i < strlen($stringField); $i++) {
            array_push($array, [
                "id" => $i,
                "value" => (int)$stringField[$i],
                "possibly" => []
            ]);
        }
        $this->allPossibly($array);
        return $array;
    }

    public function allPossibly(array &$field)
    {
        $this->cycleInit($field);
        do {
            $this->repeat = false;
            $this->segmentsSeparate($field);
        } while ($this->repeat);
    }

    public function cycleInit(array &$field)
    {
        foreach ($field as &$item) {
            if ($item['value'] === 0) {
                $item['possibly'] = [1, 2, 3, 4, 5, 6, 7, 8, 9];
            }
        }
    }

    public function segmentsSeparate(array &$field)
    {
        $this->segments = [];
        for ($i = 0; $i < 9; $i++) {
            $row = array();
            $column = array();
            $square = array();
            for ($j = 0; $j < 9; $j++) {
                $row[] = &$field[$i * 9 + $j];
                $column[] = &$field[$i + 9 * $j];
                $square[] = &$field[sudokuSquares[$i][$j]];
            }
            array_push($this->segments, $row);
            array_push($this->segments, $column);
            array_push($this->segments, $square);
        }
        foreach ($this->segments as &$item) {
            $this->existValue($item);

        }
        foreach ($this->segments as &$item) {
            $this->onlyHere($item);

        }
        foreach ($this->segments as &$item) {
            $this->onePossiblyDelete($item);
    }
    }

    public function existValue(array &$segment)
    {
        $segmentPossibly = [];
        foreach ($segment as &$item) {
            if ($item['value'] !== 0) {
                $segmentPossibly[] = $item['value'];
            }
        }
        foreach ($segment as &$item) {
            if (count(array_intersect($item['possibly'], $segmentPossibly)) !== 0) {
                $item['possibly'] = array_diff($item['possibly'], $segmentPossibly);
                $this->repeat = true;
            }
        }
    }

    public function onlyHere(array &$segment)
    {
        $segmentPossibly = [];
        foreach ($segment as $item) {
            $segmentPossibly = array_merge($segmentPossibly, $item['possibly']);
        }

        $segmentPossibly = array_keys(array_count_values($segmentPossibly), 1, true);
        foreach ($segmentPossibly as $number) {
            foreach ($segment as &$item) {
                if (in_array($number, $item['possibly'], true) && ($item['possibly']) !== [$number]) {
                    $item['possibly'] = [$number];
                    $this->repeat = true;
                }
            }
        }
    }

    public function onePossiblyDelete(array &$segment)
    {
        $segmentPossibly = [];
        foreach ($segment as $item) {
            if (count($item['possibly']) === 1) {
                $segmentPossibly[$item['id']] = current($item['possibly']);
            }
        }
        if (count($segmentPossibly) !== 0) {
            foreach ($segmentPossibly as $key => $value) {
                foreach ($segment as &$item) {

                    if ($key !== $item['id'] &&
                        count(array_intersect($item['possibly'], [$value])) !== 0) {
//                        dump($key.$item['id'] );
                        $this->repeat = true;
                        $item['possibly'] = array_diff($item['possibly'], [$value]);
                    }
                }
            }
        }
    }

    private function checkWin(array &$field): bool
    {
        $check = true;
        foreach ($field as &$item) {
            if ($item['value'] === 0 && count($item['possibly']) !== 1) {
                $check = false;
            }
        }
        return $check;
    }

    public function checkLose(array &$field): bool
    {
        $check = false;
        foreach ($field as &$item) {
            if ($item['value'] === 0 && count($item['possibly']) === 0) {
                $check = true;
            }
        }
        return $check;
    }

    public static function sudokuSolutionDifficulty2(string $stringField): int
    {
        $x = new SudokuService();
        $data = $x->fieldInit($stringField);
        $count = 0;
        foreach ($data as $item) {
            $count += count($item['possibly']);
        }
        return $count;
    }

    public static function mix(string $stringField, $count = 25)
    {
//        $rand = rand(0,1);
        $arrays = [$stringField];

//        switch ($mode) {
//            case 0:
//                break;
//            case 1:
//                $arrays = self::mix3($arrays);
//                break;
//            case 2:
//                $arrays = self::mix4($arrays);
//                break;
//        }

        $arrays = self::mix1($arrays);
        $arrays = self::mix2($arrays);
//        dump(count($arrays));
        $arrays = self::mix3($arrays);
//        dump(count($arrays));
        if (7776< $count) {
            $arrays = self::mix4($arrays);
        }
//        $arrays = self::mix4($arrays);
//        dump(count($arrays));

//        shuffle($arrays);
        //        dump($arrays);
        return array_slice($arrays,0,$count);
    }

    public static function mix3(array $array)
    {
        $arrays = $array;
        for ($i = 1; $i <= 3; $i++) {
            foreach ($arrays as $item) {
                $arrays_local = [];
                $base_array = str_split($item);
                foreach ($base_array as $k => $v) {
                    if ($k % 9 < $i * 3 && $k % 9 >= ($i - 1) * 3) {
                        $arrays_local[0][$k] = $v;
                        $arrays_local[1][($k + 1) % (3) + intdiv($k, 3) * 3] = $v;
                        $arrays_local[2][($k + 2) % (3) + intdiv($k, 3) * 3] = $v;
                    } else {
                        $arrays_local[0][$k] = $v;
                        $arrays_local[1][$k] = $v;
                        $arrays_local[2][$k] = $v;
                    }
                }
                ksort($arrays_local[1]);
                ksort($arrays_local[2]);
                foreach (str_split($item) as $k => $v) {
                    if ($k % 9 < $i * 3 && $k % 9 >= ($i - 1) * 3) {
                        if ($k % 3 === 0) {
                            $base_array[$k + 1] = $v;
                        } else if ($k % 3 === 1) {
                            $base_array[$k - 1] = $v;
                        } else {
                            $base_array[$k] = $v;
                        }
                    } else {
                        $base_array[$k] = $v;
                    }
                }
                foreach ($base_array as $k => $v) {
                    if ($k % 9 < $i * 3 && $k % 9 >= ($i - 1) * 3) {
                        $arrays_local[3][$k] = $v;
                        $arrays_local[4][($k + 1) % (3) + intdiv($k, 3) * 3] = $v;
                        $arrays_local[5][($k + 2) % (3) + intdiv($k, 3) * 3] = $v;
                    } else {
                        $arrays_local[3][$k] = $v;
                        $arrays_local[4][$k] = $v;
                        $arrays_local[5][$k] = $v;
                    }
                }
                ksort($arrays_local[4]);
                ksort($arrays_local[5]);
                foreach ($arrays_local as $k => $v) {
                    $arrays[] = implode($v);
//                    dump(implode($v));
                }
                $arrays = array_unique($arrays);
            }
        }
        return $arrays;
    }

    public static function mix4(array $array)
    {
        $arrays = [];

        foreach ($array as $item) {
            $three = str_split($item, 27);
            $count = 0;
            foreach ($three as $substr) {
                $count++;
                $data = str_split($substr, 9);
                $arrays[$count][] = $data[0] . $data[2] . $data[1];
                $arrays[$count][] = $data[0] . $data[1] . $data[2];
                $arrays[$count][] = $data[1] . $data[0] . $data[2];
                $arrays[$count][] = $data[1] . $data[2] . $data[0];
                $arrays[$count][] = $data[2] . $data[1] . $data[0];
                $arrays[$count][] = $data[2] . $data[0] . $data[1];
            }
            foreach ($arrays[1] as $substr1) {
                foreach ($arrays[2] as $substr2) {
                    foreach ($arrays[3] as $substr3) {
                        $arrays_out[] = $substr1 . $substr2 . $substr3;
                    }
                }
            }

        }

        return $arrays_out;
    }

    public static function mix2(array $array)
    {
        $arrays = [];
        foreach ($array as $item) {
            $arrays_local = [];
            $base_array = str_split($item);
            foreach ($base_array as $k => $v) {
                $arrays_local[0][$k] = $v;
                $arrays_local[1][($k + 3) % 9 + intdiv($k, 9) * 9] = $v;
                $arrays_local[2][($k + 6) % 9 + intdiv($k, 9) * 9] = $v;
            }
            ksort($arrays_local[1]);
            ksort($arrays_local[2]);
            foreach (str_split($item) as $k => $v) {
                if ($k % 9 < 3) {
                    $base_array[$k + 3] = $v;
                } else if ($k % 9 > 2 && $k % 9 < 6) {
                    $base_array[$k - 3] = $v;
                } else $base_array[$k] = $v;
            }
            foreach ($base_array as $k => $v) {
                $arrays_local[3][$k] = $v;
                $arrays_local[4][($k + 3) % 9 + intdiv($k, 9) * 9] = $v;
                $arrays_local[5][($k + 6) % 9 + intdiv($k, 9) * 9] = $v;
            }
            ksort($arrays_local[4]);
            ksort($arrays_local[5]);
            foreach ($arrays_local as $k => $v) {
                $arrays[] = implode($v);
            }
        }
        return $arrays;
    }

    public static function mix1(array $array)
    {
        $arrays = [];
        foreach ($array as $item) {
            $data = str_split($item, 27);
            $arrays[] = $data[0] . $data[2] . $data[1];
            $arrays[] = $data[0] . $data[1] . $data[2];
            $arrays[] = $data[1] . $data[0] . $data[2];
            $arrays[] = $data[1] . $data[2] . $data[0];
            $arrays[] = $data[2] . $data[1] . $data[0];
            $arrays[] = $data[2] . $data[0] . $data[1];
        }
        return $arrays;
    }

    public function newFields($count = 1, $maxClue = 17)
    {
        $arrays = [];
//        for ($i = 0; $i < $count; $i++) {
        while (count($arrays) < $count) {
            $zeros_array = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            $field = $this->fieldInit(implode($zeros_array));
            for ($j = 0; $j < rand(16, $maxClue - 1); $j++) {
                $this->setFieldValue($field);
            }
            do {
                $this->setFieldValue($field);
                $str = $this->fieldToString($field);
                $solutions = $this->sudokuSolutions($str);
                $counts = count($solutions);
            } while ($counts > 1);
            if ($counts === 1) {
                $q = [$str, reset($solutions)];
                array_push($arrays, $q);
            }
        }
        return $arrays;
    }

    public function setFieldValue(array &$field)
    {
//        dump($field[$id]);
//        if (count($field[$id]['possibly']) !== 0) {
//            $value = $field[$id]['possibly'][array_rand($field[$id]['possibly'])];
//            $field[$id]['value'] = $value;
//            $this->allPossibly($field);
//        }
        $check = false;
        while (!$check) {
            $id = $field[array_rand($field)]['id'];
            if (count($field[$id]['possibly']) !== 0) {
                $value = $field[$id]['possibly'][array_rand($field[$id]['possibly'])];
                $field[$id]['value'] = $value;
                $this->allPossibly($field);
                $check = true;
            }
        }
    }

    public function fieldToString(array &$field): string
    {
        $func = function ($value) {
            return $value['value'];
        };
        return implode(array_map($func, $field));
    }

    public static function sudokuSolutions(string $stringField): array
    {
        $methods = new SudokuService();
        $methods->solutions = [];
        $methods->Bulkhead($stringField);
        return $methods->solutions;
    }

    private function Bulkhead(string $stringField)
    {
        $field = $this->fieldInit($stringField);
        if ($this->checkWin($field)) {
            $str = '';
            foreach ($field as &$item) {
                $str .= $item['value'] > 0 ? $item['value'] : current($item['possibly']);
            }
            $this->solutions[] = $str;
        } else if (!$this->checkLose($field)) {
            $ar = array_filter($field, function ($value) {
                return count($value['possibly']) > 1;
            });
            $ar = array_shift($ar);
            $str = $stringField;
            foreach ($ar['possibly'] as $item) {
                $str[$ar['id']] = $item;
                if (count($this->solutions) < 2) {
                    $this->Bulkhead($str);
                }
            }
        }
    }
}

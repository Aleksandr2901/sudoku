<?php

namespace App\Observers;

use App\Models\User;

class UserObserver
{
    public function creating(User $user)
    {
//        $user->score = $user->loadSum('solvedSudoku as rank', 'difficulty')->rank;
//        dump($user->loadSum('solvedSudoku as rank', 'difficulty'));
//        dump($user->score);
    }

    public function updating(User $user)
    {
        $user->score = $user->loadSum('solvedSudoku as rank', 'difficulty')->rank;
        if (!$user->score) {
            $user->score = 0;
        }
//        dump($user->score);
    }
}

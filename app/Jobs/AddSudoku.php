<?php

namespace App\Jobs;

use App\Models\Sudoku;
use App\Services\SudokuService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use phpDocumentor\Reflection\Types\Integer;

class AddSudoku implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 10;
    public $timeout = 1800;

    /**
     * @var string
     */
    private $field;
    private $mode;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $field, int $mode = 0)
    {
        $this->field = $field;
        $this->mode = $mode;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sudoku = Sudoku::where("field", $this->field)
            ->first();
        if (!$sudoku) {
            $solutions = SudokuService::sudokuSolutions($this->field);
            if (count($solutions) === 1) {
                $solution = reset($solutions);
                $difficulty = SudokuService::sudokuSolutionDifficulty($this->field, $solution);
                $fields = SudokuService::mix($this->field);
                $answers = SudokuService::mix($solution);
                foreach ($fields as $key => $strfield) {
                    Sudoku::firstOrCreate(
                        ["field" => $strfield,
                            "solution" => $answers[$key], "difficulty" => $difficulty]
                    );
                }
            }
        }
    }
}

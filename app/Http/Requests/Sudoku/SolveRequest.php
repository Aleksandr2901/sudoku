<?php

namespace App\Http\Requests\Sudoku;

use Illuminate\Foundation\Http\FormRequest;

class SolveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "fields" => "array|nullable",
            "fields.*.id" =>"required_with:fields|exists:sudokus,id",
            "fields.*.solution" =>"required_with:field|string|exists:sudokus,solution",
//            'person.*.email' => 'email|unique:users',
//            'person.*.first_name' => 'required_with:person.*.last_name',
//            "categories.*" => "required_with:categories|exists:categories,id",

        ];
    }
}

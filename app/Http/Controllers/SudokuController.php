<?php

namespace App\Http\Controllers;

use App\Http\Requests\Sudoku\IndexRequest;
use App\Http\Requests\Sudoku\SolveRequest;
use App\Http\Resources\UserResource;
use App\Jobs\SudokuGenerate;
use App\Models\Sudoku;
use App\Models\User;
use Illuminate\Http\Request;

class SudokuController extends Controller
{
    public function __construct()
    {
        $this->middleware(["auth:sanctum"])
            ->only(["addSolvedSudoku"]);
    }
    public function index (IndexRequest $request) {
        $user = false;
        $fields = [];
        if ($request->userEmail) {
            $user = User::where("email", $request->userEmail)
                ->first();
        }

        for ($i=0;$i<10;$i++) {
            $sudokus = Sudoku::where("difficulty", $i)->inRandomOrder();
            if ($user) {
                $sudokus = $sudokus->whereNotIn('id',$user->solvedSudoku()->pluck("id"));
            }

//            $ar = $sudokus
//                ->take(100)
//                ->get()
//                ->toArray();


//            $fields = array_merge($fields,$ar);
//            array_push($fields, array_values($ar));

            $data[$i] = $sudokus
                ->take(100)
                ->get();
        }



//        $result = [];
//        array_walk($fields, function ($item, $key) use (&$result) {
//            $result[] = $item;
//        });

//        foreach($fields as $below){
//            $result[] = $below;
//        }

//        $fields = array_merge($fields);
//        dump($fields);
        return response()->json([
            "data" => $data
        ]);
    }

    public function addSolvedSudoku (SolveRequest $request) {
        $data = $request->validated();

        $data = $data["fields"];

        $user = $request->user();

        foreach ($data as $value) {
            if (!$user->solvedSudoku()->find($value["id"]) &&
            Sudoku::find($value["id"])->solution === $value["solution"]
            ) {
                $user->solvedSudoku()->attach($value["id"]);
            }
        }

        $user->score = $user->loadSum('solvedSudoku as rank', 'difficulty')->rank;

        $user->save();


        return response()->json([
            "message" => $user->solvedSudoku()->pluck("id"),
            "score" => $user->score
        ]);
    }
//    public function sudokuGenerate (Request $request) {
//                    SudokuGenerate::dispatch()
//                        ->delay(now()->addSeconds(1));
//
//    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\SigninRequest;
use App\Http\Requests\Auth\SignupRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function vkSignin(Request $request, $code)
    {
        //VkSigninRequest
        return response()->json([
            "message" => "hello",
//            'request' => $request,
            'code' => $code
        ]);
    }

    public function signup(SignupRequest $request)
    {
        User::create(
            $request->validated()
        );

        return response()->json([
            "message" => "User created"
        ]);
    }

    public function signin(SigninRequest $request)
    {
        $user = User::where("email", $request->email)
            ->first();

        if (!Hash::check($request->password, $user->password)) {
            abort(401, "wrong password");
        }

        $token = $user->createToken(Str::random(8))
            ->plainTextToken;

        return response()->json([
            "message" => "you are logged in",
            "token" => $token,
            "user" =>UserResource::make($user)
        ]);
    }

    public function check(Request $request)
    {
        return UserResource::make(
            $request->user()
        );
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();

        return response()->json([
            "message" => "You are logged out"
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\IndexRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use http\Env\Request;

class UserController extends Controller
{
//
//    public function __construct()
//    {
//        $this->middleware(["auth:sanctum"])
//            ->only(["update" , "destroy"]);
//    }
//

    /**
     * @param IndexRequest $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(IndexRequest $request)
    {
        $users = User::query();

        if ($request->name) {
            $users = $users->where("name", "like", "%{$request->name}%");
        }

        if ($request->email) {
            $users = $users->where("email", "like", "%{$request->email}%");
        }

        return UserResource::collection(
            $users->paginate()
        );
    }

    /**
     * @param User $user
     * @return UserResource
     */
    public function show(User $user)
    {
        return UserResource::make($user);
    }

    /**
     * @param UpdateRequest $request
     * @param User $user
     * @return UserResource
     */
    public function update(UpdateRequest $request, User $user)
    {
        $user->update(
            $request->validated()
        );

        return UserResource::make($user);
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        $user->delete();

        return response()->json([
            "message" => "\"{$user->name}\" was deleted"
        ]);
    }


    public function ranking ()
    {
        $users = User::query()
            ->orderBy('score', 'desc');

        return UserResource::collection(
            $users->paginate(25)
        );
    }
}

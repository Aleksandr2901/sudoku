<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "email" => $this->email,
            "created_at" => $this->created_at,
            "score" => $this->score,
            "rank"=> User::query()->orderBy('score', 'desc')->get()->search(function($user) {
                return $user->id === $this->id;
            }) +1
//            "updated_at" => $this->updated_at->setTimezone(
//                config("app.timezone")
//            )->toAtomString(),
//            "posts_count" => $this->posts()->count(),
//            "comment_count" => $this->comments()->count(),
        ];
    }
}
